const weatherForm = document.querySelector('form')
const searchTerm = document.querySelector('input')
const logMessage = document.querySelector('#log-message')
const forecastMessage = document.querySelector('#forecast-message')

weatherForm.addEventListener('submit', (e) => {
    e.preventDefault()
    const loc = searchTerm.value
    logMessage.textContent = 'Loading...'
    forecastMessage.textContent = ''
    fetch('/weather?address='+encodeURIComponent(loc)).then((response) => {
        response.json().then(({error, location, forecast}) => {
            if (error) { 
                logMessage.textContent = error 
                forecastMessage.textContent = ''
            }
            else {
                logMessage.textContent = location
                forecastMessage.textContent = forecast
            }
        })
    })
})