const path = require('path')
const express = require('express')
const hbs = require('hbs')

geocode = require('./utils/geocode')
forecast = require('./utils/forecast')

const app = express()
const port = process.env.PORT || 3000

// Paths for express config
const viewsPath = path.join(__dirname, '../templates/views')
const partialsPath = path.join(__dirname, '../templates/partials')

// Variable set calls for express config 
app.set('view engine', 'hbs')
app.set('views', viewsPath)
hbs.registerPartials(partialsPath)

// Setup static directory to serve
app.use(express.static(path.join(__dirname, '../public')))

app.get('', (req, res) => {
    res.render('index', {
        title: 'Weather App',
        name: 'Ghazi Majdoub'
    })
})

app.get('/about', (req, res) => {
    res.render('about', {
        title: 'About',
        name: 'Ghazi Majdoub'
    })
})

app.get('/help', (req, res) => {
    res.render('help', {
        title: 'Help',
        name: 'Ghazi Majdoub',
        message: 'If you need help, contact me by email.'
    })
})

app.get('/weather', (req, res) => {
    if (!req.query.address) {
        return res.send({
            error: 'You must provide an address!'
        })
    }

    geocode(req.query.address, (error, {lat, lon, loc} = {}) => {  
        if (error) { return res.send({ error }) }

        forecast(lat, lon, (error, fcData) => {
            if (error) {
                return res.send({ error })
            }
            return res.send({
                location: loc,
                forecast: fcData
            })
        })
    })
})

app.get('/help/*', (req, res) => {
    res.render('404', {
        title: 'Error',
        name: 'Ghazi Majdoub',
        message: 'Help article not found.'
    })
})

app.get('*', (req, res) => {
    res.render('404', {
        title: 'Error',
        name: 'Ghazi Majdoub',
        message: 'Page not found.'
    })
})

app.listen(port, () => {
    console.log('Server up on port '+port+'.')
})