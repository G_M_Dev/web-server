const request = require('request')

const forecast = (lat, lon, callback) => {
    const url = 'https://api.darksky.net/forecast/09e2ca0a816ec8325ac74e09d8c175d0/'
                +lat+','+lon+'?units=si'
    request({ url, json: true }, (error, { body }) => {
        err = undefined, forecastStr = undefined
        if(error) {
            err = 'Unable to connect to weather service!'
        } else {
            if(body.error){
                err = 'Unable to find location: ' + body.error
            } else {
                const {currently, daily} = body
                const {temperature, precipProbability} = currently
                forecastStr =   daily.data[0].summary + ' It is currently ' + 
                                temperature + ' degrees out. The high today is '+
                                daily.data[0].temperatureHigh + ' with a low of ' +
                                daily.data[0].temperatureLow + '. There is a ' + 
                                precipProbability*100 + '% chance of ' +
                                daily.data[0].precipType + '.'
            }
        }
        callback(err, forecastStr)
    })
}

module.exports = forecast